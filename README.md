## A Crossroads for the Movement: _Liberty House_

_Liberty House_ is a dedicated workspace for those of us who are devoting our time, energy, and creative resources to the movement for _liberty_ and for _health freedom_. 

In order to mobilize, communities need collaborative workspace, and we need to have our basic resources covered: That includes high-speed Internet, printers, stationary, tables, desks, workspaces. Most importantly collaborative work requires shared space that is available for long hours of the day. Consider Liberty House a crossroads, a cultural nexus, a collaboratory, a location that is set up in order to _The People_.

> The Law of Nations, "It is a settled point with writers on the natural law, that all men inherit from nature a perfect liberty and independence, of which they cannot be deprived without their own consent."

> First Amendment of the US Constitution: "Congress shall make no law respecting an establishment of religion, or prohibiting the free exercise thereof; or abridging the freedom of speech, or of the press; or **_the right of the people peaceably to assemble_**, and to petition the Government for a redress of grievances."



 in order to meet with one another and to really take the time that is necessary to drop into focused collaborative work. You can think of Liberty House as a cultural nexus, a crossroads, a _collaboratory_.





* New Media: _Truth advocates_ contextualization of 
* Collaboratory


Liberty House is a free speech, Truth and Reconciliation based information clearinghouse and new media research concern located in what was once Governor Gavin Newsoms fathers home in Nevada City, Ca :-)   

Liberty House is a community crossroads and peoples community resource exchange network designed to collect and spread the truth and create good will with all and continue doing so regardless of all the engineered social discord and a deluge of endlessly misleading racist sexist domestic propaganda encouraging the very worst in everyone. The very divisive racist sexist and public health related domestic propaganda that congress pre approved FOR ALL OF US in the US Congresses annual 2012 AUMF in those specific changes made to the 1948 Smith Mundt Act which originally RESTRICTING domestic propaganda. 

We must be prepared for ANYTHING. _We The People_ are those of us that are HERE and NOW and this QUIET but FEARLESS revolution will be won on the ground WITH our neighbors, friends and loved ones. Like minded or not. While we spread the truth seek to create common ground and create allies we must  hold those with continuing (but diminishing) power over us to account even while quickly organizing and preparing to embrace a new local libertarian economic model.

In new economic times _Liberty House_ will also serve as a barter network for the targeted exchange of goods or services, staging civic actions, growing food, alternative health and medical. Liberty House will serve as a weigh station for local and visiting luminaries with LOCAL videographers and everyone with artistic, performance or new media technology skills.



![LibertyHouse](https://ipfs.io/ipfs/QmYss8RCHagntwr2hoavJVuB4x5t35ZX9NpokJZDXKDCyd/LibertyHouse.Images.v1/CommonAreaa.jpg)

